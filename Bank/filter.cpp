#include "filter.h"
#include "bank_operations.h"
operation** filter(operation* array[], int size, bool (*check)(operation* element), int& result_size) {
    operation** result = new operation * [size];
    result_size = 0;
    for (int i = 0; i < size; i++)
    {
        if (check(array[i]))
        {
            result[result_size++] = array[i];
        }
    }
    return result;
}

/*
  <function_name>:
              ,
          true,   
    ,     

:
    array       -    
    size        -     
    check       -    .
                         
                   ,    
    result_data - ,    - ,  
                      

 
          ,  
     (     true)
*/

bool check_in_operations(operation* element) {
    return strcmp(element->operationType,"") == 0;
}

bool check_november_2021(operation* element) {
    return element->date.month == 11 && element->date.year == 21;
}

/*
  <check_function_name>:
      - ,      

:
    element -   ,   

 
    true,     ,  false   
*/
