#ifndef BANK_OPERATIONS_H
#define BANK_OPERATIONS_H

#include "constants.h"
#include <string>
struct date {
	unsigned int day, month, year;
};
struct Time {
	unsigned int second, minute, hour;
};
struct operation {
	date date;
	Time time;
	char operationType[MAX_STRING_SIZE];
	char cardNum[MAX_STRING_SIZE];
	double amount;
	char purpose[MAX_STRING_SIZE];
};
#endif // !BANK_OPERATIONS_H
